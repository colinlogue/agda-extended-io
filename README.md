# Agda Extended IO

A small extension of the Agda Standard Library that adds
some features to IO when using the GHC backend.

## Example

```agda
module HelloYou where

open import IO.Extended

hello : IO _
hello = do
  putStr "Your name: "
  flush
  name <- getLine
  putStr "Hello, "
  putStr∞ name
  putStrLn "!"

main = run hello
```