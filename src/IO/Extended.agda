-- Extends the IO module from the standard library with more options

module IO.Extended where

open import Data.Char as Char using (Char)
open import Data.List as List using ([_])
open import Data.String as String using (String)
open import Data.Unit using () renaming (⊤ to Unit)
open import Function using (_∘_)
open import Level using (Level)
open import Codata.Musical.Costring using (Costring; toCostring)

private
  variable
    a : Level
    A : Set a

module Prim where

  open import IO.Primitive

  -- toColist and fromColist are from IO.Primitive:
  -- https://agda.github.io/agda-stdlib/IO.Primitive.html

  {-# FOREIGN GHC import GHC.IO.Handle    #-}
  {-# FOREIGN GHC import GHC.IO.Handle.FD #-}

  {-# FOREIGN GHC

    fromColist :: MAlonzo.Code.Codata.Musical.Colist.AgdaColist a -> [a]
    fromColist MAlonzo.Code.Codata.Musical.Colist.Nil         = []
    fromColist (MAlonzo.Code.Codata.Musical.Colist.Cons x xs) =
      x : fromColist (MAlonzo.RTE.flat xs)

    toColist :: [a] -> MAlonzo.Code.Codata.Musical.Colist.AgdaColist a
    toColist []       = MAlonzo.Code.Codata.Musical.Colist.Nil
    toColist (x : xs) =
      MAlonzo.Code.Codata.Musical.Colist.Cons x (MAlonzo.RTE.Sharp (toColist xs))

  #-}

  postulate
    getChar  : IO Char
    getLine  : IO Costring
    flush    : IO Unit
    interact : (Costring -> Costring) -> IO Unit
    putChar  : Char -> IO Unit
  
  {-# COMPILE GHC getChar  = getChar #-}
  {-# COMPILE GHC getLine  = fmap toColist getLine #-}
  {-# COMPILE GHC flush    = hFlush stdout #-}
  {-# COMPILE GHC interact = \f -> interact (fromColist . f . toColist) #-}
  {-# COMPILE GHC putChar  = putChar #-}

open import IO public

getChar : IO Char
getChar = lift Prim.getChar

getLine : IO Costring
getLine = lift Prim.getLine

flush : IO Unit
flush = lift Prim.flush

putChar : Char -> IO Unit
putChar c = lift (Prim.putChar c)



-- lift′ is private helper from the IO module of the standard library
-- https://agda.github.io/agda-stdlib/IO.html

open import Data.Unit.Polymorphic using (⊤)

private
  open import IO.Primitive as Prim using ()
  open import Level

  lift′ : Prim.IO Unit → IO {a} ⊤
  lift′ io = lift (io Prim.>>= λ _ → Prim.return _)

interact∞ : (Costring -> Costring) -> IO {a} ⊤
interact∞ f = lift′ (Prim.interact f)

print∞ : (A -> Costring) -> A -> IO {a} ⊤
print∞ show x = putStrLn∞ (show x)

print : (A -> String) -> A -> IO {a} ⊤
print show = print∞ (toCostring ∘ show)



module NonTerminating where

  open import Agda.Builtin.Coinduction using (♭)
  open import Codata.Musical.Colist as Colist using ()

  {-# NON_TERMINATING #-}
  fromCostring : Costring -> String
  fromCostring (Colist.[]) = ""
  fromCostring (c Colist.∷ cs) = (String.fromList [ c ]) String.++ fromCostring (♭ cs)

  {-# NON_TERMINATING #-}
  interact : (String -> String) -> IO {a} ⊤
  interact f = interact∞ (toCostring ∘ f ∘ fromCostring)

open NonTerminating public